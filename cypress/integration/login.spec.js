/// <reference types="cypress" />

describe('Login', () => {
    beforeEach(() => {
        cy.server()
        cy.visit('http://localhost:4200/auth/login')
    })
    
    it('Should login when providing correct email and password.', () => {
        cy.route('POST', '**/user/login', 'fixture:login.json')
        cy.get('#exampleInputEmail1').type('afrash.ramjit@hotmail.nl')
        cy.get('#exampleInputPassword1').type('secret')

        cy.route('GET', '**/match', 'fixture:matches.json')

        cy.route('GET', '**/match/602aa298d0cf561bdccbe60c', 'fixture:match.json')

     
        cy.route('GET', '**/fighter/5fc7e749aa47921750c739f7', 'fixture:firstFighter.json')
        cy.route('GET', '**/fighter/5fc7e77faa47921750c739f9', 'fixture:secondFighter.json')
        

        cy.get('#submitButtonForLogin').click();

        cy.get(' tr > :nth-child(2) > a').click();
       

    })
})