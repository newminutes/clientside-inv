/// <reference types="cypress" />

describe('Login', () => {
    beforeEach(() => {
        cy.server()
        cy.visit('http://localhost:4200/auth/login')

        cy.route('GET', '**/match', 'fixture:matches.json')

        cy.route('GET', '**/match/602aa298d0cf561bdccbe60c', 'fixture:match.json')
        cy.route('POST', '**/user/login', 'fixture:login.json')
        cy.get('#exampleInputEmail1').type('afrash.ramjit@hotmail.nl')
        cy.get('#exampleInputPassword1').type('secret')

        // cy.route('GET', '**/match', 'fixture:matches.json')

        // cy.route('GET', '**/match/602aa298d0cf561bdccbe60c', 'fixture:match.json')
        

        cy.get('#submitButtonForLogin').click();

   
        
    })
    

    it('Should update fighter.', () => {

        cy.route('GET', '**/5fc7e749aa47921750c739f7/edit', 'fixture:firstFighter.json')
        cy.route('GET', '**/fighter/5fc7e749aa47921750c739f7', 'fixture:firstFighter.json')

        cy.route('GET', '**/match', 'fixture:matches.json')

        cy.route('GET', '**/match/602aa298d0cf561bdccbe60c', 'fixture:match.json')

        //get all fighters
        cy.route('GET', '**/fighter', 'fixture:fighters.json')
        cy.route('GET', '**/fighter/5fc7e733aa47921750c739f6', 'fixture:firstFighter.json')

        cy.get(':nth-child(3) > .nav-link').click();

        cy.get(' tbody > :nth-child(1) > :nth-child(2) > a').click();

        

        cy.get('#canEditFighter').click();


        //edit the fighter
        cy.get('#editMatchName').type('testFighter')
        cy.get('#EditmatchLocation').type('secret')
        cy.get('#EditMatchAddress').type('secret')

        cy.route('PUT', '**/fighter/5fc7e749aa47921750c739f7', 'fixture:firstFighter.json')

        cy.get(':nth-child(7) > .btn').click();
        
    })
})