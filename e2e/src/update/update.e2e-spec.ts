
import { browser, logging, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';
import { UpdatePage } from './update.po';

/**
 * https://www.youtube.com/watch?v=ympTE-bLYaU
 */
describe('Login page', () => {
  let page: UpdatePage;
  

  /**
   *
   */
  beforeEach(() => {
    page = new UpdatePage();
  });

  /**
   *
   */
  it('should be at /login route after initialisation', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('register/login');
    expect(browser.getCurrentUrl()).toContain('register/login');
  });

  it('should be at matches route ', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/matches');
    expect(browser.getCurrentUrl()).toContain('/matches');
  });

  it('should have create button ', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('matches/createMatches');
    expect(browser.getCurrentUrl()).toContain('register/login');
  });

  it('should login successfully when provided with a valid email and password', () => {
    // browser.waitForAngularEnabled(false);
    // browser.get('http://localhost:4200/register/login');
    // page.navigateTo('/register/login');

    // page.emailInput.sendKeys('afrash.ramjit@hotmail.nl');
    // page.passwordInput.sendKeys('secret');
    // expect(page.submitButton.isEnabled()).toBeTrue;

    // page.submitButton.click();

    // browser.driver.sleep(500);
    // expect(browser.getCurrentUrl()).toContain('/matches');
    

    // tslint:disable-next-line: quotemark
    // const getStoredUser = "return window.localStorage.getItem('currentuser');";
    // browser.executeScript(getStoredUser).then((user) => {
    //   expect(user).toBeTruthy();
    //   expect(user).toContain('afrash.ramjit@hotmail.nl');
    // });

  });


  it('should login successfully when provided with a valid email and password', () => {
    // browser.waitForAngularEnabled(false);
    // browser.get('http://localhost:4200/register/login');
    // page.navigateTo('/register/login');

    // page.emailInput.sendKeys('afrash.ramjit@hotmail.nl');
    // page.passwordInput.sendKeys('secret');
    // expect(page.submitButton.isEnabled()).toBeTrue;

    // page.submitButton.click();

    // browser.driver.sleep(500);
    // expect(browser.getCurrentUrl()).toContain('/matches');
    

    // // tslint:disable-next-line: quotemark
    // const getStoredUser = "return window.localStorage.getItem('currentuser');";
    // browser.executeScript(getStoredUser).then((user) => {
    //   expect(user).toBeTruthy();
    //   expect(user).toContain('afrash.ramjit@hotmail.nl');
    // });

    // browser.get('http://localhost:4200/matches/createMatches');
    // expect(browser.getCurrentUrl()).toContain('/matches/createMatches');
  });

  // it('should be at detail path of match ', () => {
  //   browser.waitForAngularEnabled(false);
  //   page.navigateTo('matches/5fceb30dfd345645d89553db');
  //   expect(browser.getCurrentUrl()).toContain('matches/5fceb30dfd345645d89553db');
  // });

  // it('should go to create ', () => {
  //   browser.waitForAngularEnabled(false);
  //   page.navigateTo('matches/5fceb30dfd345645d89553db');
  //   expect(browser.getCurrentUrl()).toContain('matches/5fceb30dfd345645d89553db');
  // });
//   it('should have edit button ', () => {
//     browser.waitForAngularEnabled(false);
//     page.navigateTo('matches/5fceb30dfd345645d89553db');
//     expect(browser.getCurrentUrl()).toContain('matches/5fceb30dfd345645d89553db');
//     page.editButtonMatch.click();
//   });

 
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    // const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    // expect(logs).not.toContain(
    //   jasmine.objectContaining({
    //     level: logging.Level.SEVERE,
    //   } as logging.Entry)
    // );

    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.removeItem('currentuser')");
  });
});
