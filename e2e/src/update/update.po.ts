import { by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../common.po';

export class UpdatePage extends CommonPageObject {

  get  editButtonMatch(): ElementFinder {
    return element(by.id('editButtonMatch')) as ElementFinder;
  }

  get emailInput(): ElementFinder {
    return element(by.id('exampleInputEmail1')) as ElementFinder;
  }

  get passwordInput(): ElementFinder {
    return element(by.id('exampleInputPassword1')) as ElementFinder;
  }

  get submitButton(): ElementFinder {
    return element(by.id('submitButtonForLogin')) as ElementFinder;
  }

  get createMatch(): ElementFinder {
    return element(by.id('createMatch')) as ElementFinder;
  }

  get passwordInvalidMessage(): ElementFinder {
    return element(by.id('password-invalid')) as ElementFinder;
  }
}
