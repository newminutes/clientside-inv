import { by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../common.po';

export class LoginPage extends CommonPageObject {
  get emailInput(): ElementFinder {
    return element(by.id('exampleInputEmail1')) as ElementFinder;
  }

  get passwordInput(): ElementFinder {
    return element(by.id('exampleInputPassword1')) as ElementFinder;
  }

  get submitButton(): ElementFinder {
    return element(by.id('submitButtonForLogin')) as ElementFinder;
  }

//   get emailInvalidMessage(): ElementFinder {
//     return element(by.id('exampleInputEmail1')) as ElementFinder;
//   }

//   get passwordInvalidMessage(): ElementFinder {
//     return element(by.id('exampleInputPassword1')) as ElementFinder;
//   }
}
