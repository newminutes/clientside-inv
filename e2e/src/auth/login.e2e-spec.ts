import { LoginPage } from './login.po';
import { browser, logging, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';

/**
 * https://www.youtube.com/watch?v=ympTE-bLYaU
 */
describe('Login page', () => {
  let page: LoginPage;

  /**
   *
   */
  beforeEach(() => {
    page = new LoginPage();
  });

  /**
   *
   */
  it('should be at /login route after initialisation', () => {
    browser.waitForAngularEnabled(false);
    browser.get('http://localhost:4200/register/login');
    page.navigateTo('register/login');
    expect(browser.getCurrentUrl()).toContain('register/login');
    expect(page.submitButton.isEnabled()).toBeFalse;
  });

  /**
   *
   */
  it('should display correct error messages when no values are entered in inputs', () => {
    browser.waitForAngularEnabled(false);
    browser.get('http://localhost:4200/register/login');
    page.navigateTo('register/login');

    page.emailInput.click();
    page.passwordInput.click();

    // https://stackoverflow.com/a/52782814/3471923
    page.emailInput.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.emailInput.sendKeys(protractor.Key.BACK_SPACE);
    page.passwordInput.sendKeys(
      protractor.Key.chord(protractor.Key.CONTROL, 'a')
    );
    page.passwordInput.sendKeys(protractor.Key.BACK_SPACE);

    browser.driver.sleep(100);

    //expect(page.emailInvalidMessage).toBeTruthy();
    // expect(page.emailInvalidMessage.getText()).toContain(
    //   'enter a valid email address'
    // );
    // expect(page.passwordInvalidMessage).toBeTruthy();
    // expect(page.passwordInvalidMessage.getText()).toContain(
    //   'enter a valid password'
    // );
    expect(page.submitButton.isEnabled()).toBeFalse;
    expect(browser.getCurrentUrl()).toContain('localhost:4200/register/login');
  });

  /**
   * Promised approach
   */
  it('should login successfully when provided with a valid email and password', () => {
    browser.waitForAngularEnabled(false);
    browser.get('http://localhost:4200/register/login');
    page.navigateTo('/register/login');

    page.emailInput.sendKeys('afrash.ramjit@hotmail.nl');
    page.passwordInput.sendKeys('secret');
    expect(page.submitButton.isEnabled()).toBeTrue;

    page.submitButton.click();

    // browser.driver.sleep(500);
    // expect(browser.getCurrentUrl()).toContain('/matches');
    

    // tslint:disable-next-line: quotemark
    const getStoredUser = "return window.localStorage.getItem('currentuser');";
    browser.executeScript(getStoredUser).then((user) => {
      expect(user).toBeTruthy();
      expect(user).toContain('afrash.ramjit@hotmail.nl');
    });

    browser.get('http://localhost:4200/matches/createMatches');
    expect(browser.getCurrentUrl()).toContain('/matches/createMatches');
  });

  /**
   *
   */
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    // expect(logs).not.toContain(
    //   jasmine.objectContaining({
    //     level: logging.Level.SEVERE,
    //   } as logging.Entry)
    // );

    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.removeItem('currentuser')");
  });
});
