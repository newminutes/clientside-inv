import { AppPage } from '../app.po';
import { browser, element, logging, by } from 'protractor';
import { ReadPage } from './read.po';

describe('workspace-project App', () => {
  let page: ReadPage;

  beforeEach(() => {
    page = new ReadPage();
  });

  it('should go to login page', async () => {
    await page.navigateTo();
    expect(browser.getCurrentUrl()).toContain('register/login');
  });

it('should go to about page', async () => {
    browser.get('http://localhost:4200/about');
    expect(browser.getCurrentUrl()).toContain('http://localhost:4200/about');
  });

  it('should go to users page', async () => {
    browser.waitForAngularEnabled(false);
    browser.get('http://localhost:4200/users');
    expect(browser.getCurrentUrl()).toContain('http://localhost:4200/users');
  });

  it('should go to user detail page', async () => {
    browser.waitForAngularEnabled(false);
    browser.get('http://localhost:4200/users/5fc788564466d807d04f553f');
    // await page.getById;
    
    expect(browser.getCurrentUrl()).toContain('http://localhost:4200/users/5fc788564466d807d04f553f');
  });

  it('should have a name tag and a email tag', async () => {
    browser.waitForAngularEnabled(false);
    browser.get('http://localhost:4200/users/5fc788564466d807d04f553f');
    // await page.getById;
    
    expect(browser.getCurrentUrl()).toContain('http://localhost:4200/users/5fc788564466d807d04f553f');
    expect(element(by.id('nameTag'))).toBeTruthy();
    expect(element(by.id('emailTag'))).toBeTruthy();
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    // expect(logs).not.toContain(
    //   jasmine.objectContaining({
    //     level: logging.Level.SEVERE,
    //   } as logging.Entry)
    // );

    // tslint:disable-next-line: quotemark
    //browser.executeScript("window.localStorage.removeItem('currentuser')");
  });
});
