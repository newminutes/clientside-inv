import { by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../common.po';


export class ReadPage extends CommonPageObject {
  get getById(): ElementFinder {
    return element(by.id("getUserById")) as ElementFinder;
  }
  get nameTag(): ElementFinder {
    return element(by.id("nameTag")) as ElementFinder;
  }
  get emailTag(): ElementFinder {
    return element(by.id("emailTag")) as ElementFinder;
  }
  get username(): ElementFinder {
    return element(by.id("username")) as ElementFinder;
  }


}
