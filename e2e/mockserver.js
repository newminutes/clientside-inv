//
// This server will be started by Protractor in end-to-end tests.
// Add your API mocks for your specific project in this file.
//
const express = require("express");
var cors = require('cors')
const port = 3000;

let app = express();
let routes = require("express").Router();

var corsOptions = {
  origin: 'http://example.com',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

// Add CORS headers so our external Angular app is allowed to connect
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

// routes.post("user/login", (req, res, next) => {
//   res.status(200).json({
//     _id: "5fc788564466d807d04f553f",
//     email: "afrash.ramjit@hotmail.nl",
//     accessToken:
//       "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZmM3ODg1NjQ0NjZkODA3ZDA0ZjU1M2YiLCJpYXQiOjE2MDc0NzMwMzIsImV4cCI6MTYwNzU1OTQzMn0.r6Zyg4kzS26seItwIuFs-W8YI6U9tBvBKNXa2qmHDp0",
//   });
// });

routes.post("/api/fighter", (req, res, next) => {
  res.status(200).json({
    _id: "5fc7e733aa47921750c739f6",
        name: "Roger Gracie",
        fightstyle: "Pressure",
        sex: "Male",
        __v: 0
   }, { 
    matches: [],
    _id: "5fc7e749aa47921750c739f7",
    name: "Buchecha Almeida",
    fightstyle: "Pressure",
    sex: "Male",
    __v: 0
   });
});



routes.post('/matches/createMatch', (req, res, next) => {
  res.status(200).json({
    favoriteMatches: [],
    _id: "5fceb30dfd345645d89553db",
    name: "testvoorupdate",
    country: "Algeria",
    location: "ggg",
    address: "gggg",
    dateAndTime: "2020-12-18T23:59",
    matchMadeByName: "afrash.ramjit@hotmail.nl",
    comments: [],
    __v: 0
  });
});




routes.post('user/login', (req, res, next) => {
  res.status(200).json({
      applied: [],
      _id: "5fc788564466d807d04f553f",
      name: 'Afrash',
      email: "afrash.ramjit@hotmail.nl",
      accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZmM3ODg1NjQ0NjZkODA3ZDA0ZjU1M2YiLCJpYXQiOjE2MDc1MzIwNTEsImV4cCI6MTYwNzYxODQ1MX0.RYxIhp4jd3D2YX3_zMf6MmLRNhHVLVvZiShRgP7SFQA"
    });
});



//
// Write your own mocking API endpoints here.
//

// Finally add your routes to the app
app.use(routes);

app.use("*", function (req, res, next) {
  next({ error: "Non-existing endpoint" });
});

app.use((err, req, res, next) => {
  res.status(400).json(err);
});

app.listen(port, () => {
  console.log("Mock backend server running on port", port);
});
