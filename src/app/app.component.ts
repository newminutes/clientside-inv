import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'my-app';

  constructor(){

  }

  // moment waarop scherm in de lucht komt.
  ngOnInit(): void {
    console.log('Appcomponent geladen');
  }

}
