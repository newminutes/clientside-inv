import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../../../auth/auth-service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
})
export class UserDetailComponent implements OnInit, OnDestroy {
  user$: Observable<any>;
  user = new User();
  subscription: Subscription;
  id = '';
  private routeSub: Subscription;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    public authService: AuthService, private router: Router
  ) {}

  ngOnInit(): void {
    console.log('user detail called');

    this.user$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap)=>
      this.userService.getById(params.get('id'))
      )
    );


    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });


    this.subscription = this.userService.getById(this.id)
    .subscribe(data => {
      console.log(data);
      this.user = data;
    });
    
  }

  tryToEdit(){
    console.log('console called');
    this.router.navigateByUrl('users/editUser/' + this.user._id);
  }
  

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }

    if(this.routeSub){
      this.routeSub.unsubscribe();
    }
  }
}
