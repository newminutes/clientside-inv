import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { delay, filter, mergeMap, take } from 'rxjs/operators';
import { User } from './user.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {


  constructor(private http: HttpClient) {}

  

  getAll(): Observable<any[]> {
    return this.http.get<any>(environment.apiUrl + 'user');
  }

  getById(id: string): Observable<any> {
    return this.http.get<any>(environment.apiUrl + `user/${id}`);
  }

  
}
