import { Fighter } from '../fighter/fighter.model';
export class Match {
    _id: string;
    matchMadeBy: string;
    matchMadeByName: string;
    name: string;
    location: string;
    country: string;
    comments?: Array<any>;
    firstFighter: Fighter;
    secondFighter: Fighter;
    dateAndTime: string;
    address: string;
}
