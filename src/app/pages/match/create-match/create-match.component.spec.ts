import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMatchComponent } from './create-match.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CreateMatchComponent', () => {
  let component: CreateMatchComponent;
  let fixture: ComponentFixture<CreateMatchComponent>;

  beforeEach( () => {
    TestBed.configureTestingModule({
     imports: [RouterTestingModule, HttpClientTestingModule, FormsModule ],
     declarations: [ CreateMatchComponent ]
   })
   .compileComponents();
    fixture = TestBed.createComponent(CreateMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
 });



  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
