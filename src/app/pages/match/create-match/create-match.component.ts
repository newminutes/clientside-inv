import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatchService } from '../match.service';
import { Match } from '../match.model';
import { Router } from '@angular/router';
import { AuthService } from '../../../auth/auth-service';
import { Fighter } from '../../fighter/fighter.model';
import { Observable, Subscription } from 'rxjs';
import { countryList } from '../country';


@Component({
  selector: 'app-create-match',
  templateUrl: './create-match.component.html',
  styleUrls: ['./create-match.component.css']
})
export class CreateMatchComponent implements OnInit, OnDestroy {


  constructor(private matchService: MatchService, private router: Router, public authService: AuthService) { }

  public selectedValue: string;
  public selectedFirstFighter: Fighter;
  public selectedSecondFighter: Fighter;
  public countries = countryList;
  createMatchSubscription: Subscription;
  getAllFightersSubscription: Subscription;
  fighters$: Observable<Fighter[]>;

  match = new Match();

  ngOnInit(): void {
    console.log('fighters geladen');
    this.getAllFighters();
  }

  createMatch() {
    console.log('create match called');
    console.log(this.selectedFirstFighter._id);
    console.log(this.selectedSecondFighter._id);

    this.match.firstFighter = this.selectedFirstFighter;
    this.match.secondFighter = this.selectedSecondFighter;


    this.match.matchMadeBy = this.authService.currentUser$.value._id;
    this.match.matchMadeByName = this.authService.currentUser$.value.email;
    this.createMatchSubscription = this.matchService.createMatch(this.match)
    .subscribe(data => {
      console.log(data);
      this.router.navigate(['/matches']);
    });
  }

  getAllFighters(){
    this.getAllFightersSubscription = this.matchService.getAll().subscribe(fighters => {
      this.fighters$ = this.matchService.getAllFighters();
    });
   }

   ngOnDestroy(): void {
    if(this.createMatchSubscription){
      this.createMatchSubscription.unsubscribe();
    }
    if(this.getAllFightersSubscription){
      this.getAllFightersSubscription.unsubscribe();
    }
  }
}
