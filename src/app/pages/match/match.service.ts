import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { from, Observable, of, throwError } from 'rxjs';
import { catchError, delay, filter, mergeMap, take } from 'rxjs/operators';
import { Match } from '../match/match.model';
import { environment } from '../../../environments/environment';
import { Fighter } from '../fighter/fighter.model';
import { Comment } from '../about/comment/comment.model';



@Injectable({
  providedIn: 'root',
})

export class MatchService {


    constructor(private http: HttpClient) {}

    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong.
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      // Return an observable with a user-facing error message.
      return throwError(
        'Something bad happened; please try again later.');
    }

    getAll(): Observable<any> {
      return this.http.get(environment.apiUrl + 'match');
    }

    getById(id: string): Observable<any> {
      return this.http.get<any>(environment.apiUrl + `match/${id}`);
    }

    createMatch(match: Match): Observable<any> {
      const headers = { 'content-type': 'application/json'}
      return this.http.post<any>(environment.apiUrl + 'match', match, {headers});
    }

    updateMatch(id: string, match: Match): Observable<any> {
      const headers = { 'content-type': 'application/json'};
      return this.http.put<any>(environment.apiUrl + `match/${id}`, match, {headers});
    }

    deleteMatch(match: Match): Observable<any> {
      console.log('delete called');
      return this.http.delete<any>(environment.apiUrl + `match/${match._id}`);
    }


    getAllFighters(): Observable<any> {
      return this.http.get(environment.apiUrl + 'fighter');
    }

    getFighterById(fighter: Fighter): Observable<any> {
      return this.http.get<any>(environment.apiUrl + `fighter/${fighter}`);
    }

    
    getCommentById(id: string): Observable<Comment> {
      return this.http.get<Comment>(environment.apiUrl + `comment/${id}`);
    }
  
    createComment(comment: Comment): Observable<any> {
      const headers = { 'content-type': 'application/json'}
      return this.http.post<any>(environment.apiUrl + 'comment', comment, {headers});
    }

}
