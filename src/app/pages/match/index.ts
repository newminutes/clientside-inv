import { MatchDetailComponent } from './match-detail/match-detail.component';
import { MatchEditComponent } from './match-edit/match-edit.component';
import { MatchListComponent } from './match-list/match-list.component';
import { CreateMatchComponent } from './create-match/create-match.component';


export const components: any[] = [
  MatchDetailComponent,
  MatchEditComponent,
  MatchListComponent,
  CreateMatchComponent,
];

export * from '../match/match-list/match-list.component';
export * from '../match/match-detail/match-detail.component';
export * from '../match/match-edit/match-edit.component';
export * from '../match/create-match/create-match.component';
