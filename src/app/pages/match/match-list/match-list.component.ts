import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Observable, from, Subscription } from 'rxjs';
import { Match } from '../match.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { MatchService } from '../match.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../auth/auth-service';

@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.css']
})
export class MatchListComponent implements OnInit, OnDestroy {
  matches$: Observable<Match[]>;
  deleteMatchSubscription: Subscription;
  getAllMatchSubscription: Subscription;
  constructor(private matchservice: MatchService, private route: ActivatedRoute,
              public authService: AuthService) { }
  

  ngOnInit(): void {
    console.log('Matches geladen');
    this.getAll();
 }

 getAll(){
  this.getAllMatchSubscription = this.matchservice.getAll().subscribe(matches => {
    this.matches$ = this.matchservice.getAll();
  });
 }


 deleteMatch(match: Match){
  this.deleteMatchSubscription = this.matchservice.deleteMatch(match).subscribe(data=>{
      console.log(data);
      this.getAll();
    });

}

ngOnDestroy(): void {
  if(this.deleteMatchSubscription){
    this.deleteMatchSubscription.unsubscribe();
  }
  if(this.getAllMatchSubscription){
    this.getAllMatchSubscription.unsubscribe();
  }
}

}




