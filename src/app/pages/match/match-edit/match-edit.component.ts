import { Component, OnInit, OnDestroy } from '@angular/core';
import { Match } from '../match.model';
import { MatchService } from '../match.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AuthService } from '../../../auth/auth-service';
import { Fighter } from '../../fighter/fighter.model';
import { countryList } from '../country';
import { Observable, Subscription, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { FighterService } from '../../fighter/fighter.service';

@Component({
  selector: 'app-match-edit',
  templateUrl: './match-edit.component.html',
  styleUrls: ['./match-edit.component.css']
})
export class MatchEditComponent implements OnInit, OnDestroy {
  
  subscription: Subscription;

  public selectedValue: string;
  public countries = countryList;

  public matchPreDefined = new Match();

  fighters$: Observable<Fighter[]>;
  id = this.route.snapshot.params.id;
  match = new Match();
  subscriptionoptions: Subscription;
  subscriptionParams: Subscription;


  constructor(private matchService: MatchService, private route: ActivatedRoute,
              private router: Router,
              public authService: AuthService, public fightService: FighterService) { }
  

  ngOnInit(): void {
    this.fighters$ = this.fightService.getAll();
    console.log('fighters geladen');
    // this.matches$ = this.modelService.getAll();
    // this.getMatchById();
    this.subscription = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.matchService.getById(params.get('id'));
      })
    ).pipe(
    ).subscribe(data => {
      this.match = data;
    })

    this.getAllFighters();
  }

  updateMatch() {
    console.log('update match called');
    console.log(this.id);

    this.match.matchMadeBy = this.authService.currentUser$.value._id;
    this.subscriptionParams = this.matchService.updateMatch(this.id, this.match)
    .subscribe(data => {
      console.log(data);
      this.router.navigate(['/matches']);
    });
  }

  getAllFighters(){
    // gets all matches
   this.subscriptionoptions = this.matchService.getAll().subscribe(fighters => {
     // gets all fighters
      this.fighters$ = this.matchService.getAllFighters();
    });
   }

  ngOnDestroy(): void {
    if(this.subscriptionoptions){
      this.subscriptionoptions.unsubscribe();
    }
    if(this.subscriptionParams){
      this.subscriptionParams.unsubscribe();
    }
  }
}
