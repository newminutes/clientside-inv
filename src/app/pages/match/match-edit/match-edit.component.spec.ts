import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchEditComponent } from './match-edit.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Component, Directive, HostListener, Input } from '@angular/core';
import { Match } from '../match.model';
import { Fighter } from '../../fighter/fighter.model';
import { User } from '../../user/user.model';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { AuthService } from '../../../auth/auth-service';
import { AlertService } from '../../../alert/alert.service';
import { MatchService } from '../match.service';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { FighterService } from '../../fighter/fighter.service';


const firstFighter: Fighter = {
  _id: '5',
    fightstyle: 'pressure',
    name: 'mendez',
    sex: 'male',
    fighterMadeBy: 'user@host.com'
};

const fighters: Fighter[] = [{ _id: '5',
fightstyle: 'pressure',
name: 'mendez',
sex: 'male', },
 {_id: '6',
fightstyle: 'pressure',
name: 'rafa',
sex: 'male',
fighterMadeBy: 'user@host.com'
}] as Fighter[];
 

const secondFighter: Fighter = {
  _id: '6',
    fightstyle: 'pressure',
    name: 'rafa',
    sex: 'male',
    fighterMadeBy: 'user@host.com'
};

// Global mock objects
const expectedMatch: Match = {
  _id: '2',
  matchMadeBy: '1',
  matchMadeByName: 'user@host.com',
  name: 'test',
  location: 'test',
  country: 'test',
  firstFighter,
  secondFighter,
  dateAndTime: 'test',
  address: 'test'
};

const newExpectedMatch: Match = {
    _id: '5',
    matchMadeBy: '1',
    matchMadeByName: 'user@host.com',
    name: 'test',
    location: 'test',
    country: 'test',
    firstFighter,
    secondFighter,
    dateAndTime: 'test',
    address: 'test'
  };

const expectedUserData: User = {
  _id: '1',
  name: 'afrash',
  email: 'user@host.com',
  password: 'test',
  accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZmM2NDQxZDE0NjcyNzM0MTAyZTE0ZWUiLCJpYXQiOjE2MDY4MjkwODUsImV4cCI6MTYwNjkxNTQ4NX0.n1v9v7-5TM5PpGMulA6UUxGDMJp3CXAfej_2J1Ia9qA',
};


describe('MatchEditComponent', () => {
  let component: MatchEditComponent;
  let fixture: ComponentFixture<MatchEditComponent>;
  
  let matchServiceSpy;
  let authServiceSpy;
  let routerSpy;

 

  beforeEach(() => {
   
    authServiceSpy = jasmine.createSpyObj(
      'AuthService',
      [
        'login',
        'register',
        'logout',
        'getUserFromLocalStorage',
        'saveUserToLocalStorage',
        'userMayEdit',
      ]
      // ['currentUser$']
    );
    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authServiceSpy.currentUser$ = mockUser$;

    matchServiceSpy = jasmine.createSpyObj('MatchService', ['getById', 'getAll', 'getAllFighters', 'updateMatch']);
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

    TestBed.configureTestingModule({
      // The declared components needed to test the UsersComponent.
      declarations: [
        MatchEditComponent, // The 'real' component that we will test
       
      ],
      imports: [FormsModule, RouterTestingModule, HttpClientModule],
      //
      // The constructor of our real component uses dependency injected services
      // Never provide the real service in testcases!
      //
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        // { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: MatchService, useValue: matchServiceSpy },
        // { provide: FighterService, useValue: fighterServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {snapshot: {params: {id: '1'}}}
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MatchEditComponent);
    component = fixture.componentInstance;
  });

  /**
   *
   */
  afterEach(() => {
    fixture.destroy();
  });

  /**
   *
   */
//   it('should create', (done) => {
//     matchServiceSpy.getById.and.returnValue(of(expectedMatch));
//     matchServiceSpy.getAll.and.returnValue(of(expectedMatch));
//     matchServiceSpy.getAllFighters.and.returnValue(of(fighters));
//     // Deze zijn nodig zodat we in ngOnDestroy kunnen unsubsciben.
//     component.subscriptionoptions = new Subscription();
//     component.subscriptionParams = new Subscription();
//     component.subscriptionoptions = new Subscription();

//     fixture.detectChanges();
//     expect(component).toBeTruthy();

//     setTimeout(() => {
//       done();
//     }, 200);
//   });
it('should be able to edit an match', () => {
    authServiceSpy.getUserFromLocalStorage.and.returnValue(of(expectedUserData));
    component.match = expectedMatch;
    component.subscription = new Subscription();
    console.log(expectedMatch);
    expect(component.match).toEqual(expectedMatch);
    expect(authServiceSpy.currentUser$.value).toEqual(expectedUserData);
    component.match.name = 'test2';
    expect(component.match).toEqual(expectedMatch);
    // console.log(component.artist)
    matchServiceSpy.updateMatch.withArgs(component.match._id, component.match).and.returnValue(of(newExpectedMatch));
    console.log(component.match);
    expect(component.match).toEqual(expectedMatch);
  });

  


});