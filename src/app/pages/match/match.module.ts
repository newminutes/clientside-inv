import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchEditComponent } from './match-edit/match-edit.component';
import { CreateMatchComponent } from './create-match/create-match.component';
import { MatchDetailComponent } from './match-detail/match-detail.component';
import * as fromComponents from '.';
import { MatchListComponent } from './match-list/match-list.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { LoggedInAuthGuard } from '../../auth/logged-in-auth-guard';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';

const routes: Routes = [
  {
    path: ':id/edit', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: MatchEditComponent,
  },
  {
    path: 'create', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: CreateMatchComponent,
  },
  { path: '', pathMatch: 'full', component: MatchListComponent },
  {
    path: ':id', pathMatch: 'full', component: fromComponents.MatchDetailComponent,
  },


];

@NgModule({
  declarations: [MatchEditComponent, CreateMatchComponent, MatchDetailComponent],
  imports: [
    CommonModule,
    FormsModule ,
    MatFormFieldModule, MatSelectModule,
    RouterModule.forChild(routes)
  ]
})
export class MatchModule { }
