import { Component, OnInit } from '@angular/core';
import { MatchService } from '../match.service';
import { Match } from '../match.model';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { switchMap, map, tap, mergeMap, delay } from 'rxjs/operators';
import { AuthService } from '../../../auth/auth-service';
import { Fighter } from '../../fighter/fighter.model';
import { LoginComponent } from '../../../auth/login/login.component';
import { LoggedInAuthGuard } from '../../../auth/logged-in-auth-guard';
import { element } from 'protractor';
import { Comment } from '../../about/comment/comment.model';

@Component({
  selector: 'app-match-detail',
  templateUrl: './match-detail.component.html',
  styleUrls: ['./match-detail.component.css']
})
export class MatchDetailComponent implements OnInit {
  match$: Observable<Match>;
  newMatch = new Match();
  test$: Observable<any>;
  comments = new Match();
  firstFighter$:  Subscription;
  firstFighter = new Fighter();
  secondFighter = new Fighter();
  secondFighter$: Subscription;
  firstFighterName = '';
  secondFighterName = '';
  fighter = new Fighter();
  id = '';
  isAuthenticated: boolean;
  private routeSub: Subscription;
  subscriptionForAuth: Subscription;

  commentslist: Comment[] = []
  commentslist$: Subscription;


  constructor(private matchservice : MatchService, private route: ActivatedRoute,  public authService: AuthService,
              private router: Router, public logged : LoggedInAuthGuard) {}

  ngOnInit(): void {
    
    this.match$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap)=>
        this.matchservice.getById(params.get('id'))
      )
    );
    
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    this.matchservice.getById(this.id)
    .subscribe(data => {
      console.log(data);
      this.newMatch = data;
      this.commentslist = [];
      this.newMatch.comments.forEach(element =>{
        // console.log("TEST: , ", element)
        this.commentslist$ = this.matchservice.getCommentById(element.toString()).subscribe(items => {
          console.log("ITEMS: ", items)
          this.commentslist.push(items)
          
          
        });
        delay(1500);
      });

     console.log("These will be the comments: ", this.commentslist)
    });
    

    this.firstFighter$ = this.matchservice.getById(this.id).pipe(
      switchMap(element => this.matchservice.getFighterById(element.firstFighter))
    )
    .subscribe(items => {
      this.firstFighter$ = items;
      console.log(items);
      this.firstFighter = items;
    })

    this.secondFighter$ = this.matchservice.getById(this.id).pipe(
      switchMap(element => this.matchservice.getFighterById(element.secondFighter))
    )
    .subscribe(items => {
      this.secondFighter$ = items;
      console.log(items);
      this.secondFighter = items;
    })

  }



  deleteMatch(){
    console.log('name: ', this.newMatch.name)
    this.matchservice.deleteMatch(this.newMatch).subscribe(data=>{
      console.log(data);
      this.router.navigateByUrl('matches');
    });
  }

  tryToEdit(){
    
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if("["+JSON.stringify(this.authService.currentUser$.value._id)+"]" === JSON.stringify(this.newMatch.matchMadeBy)){
        console.log(' allowed');
        this.router.navigateByUrl('matches/' + this.newMatch._id + '/edit');
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log("["+JSON.stringify(this.authService.currentUser$.value._id)+"]");
        this.router.navigateByUrl('auth/login');
      }
    }
  }

  tryToDelete(){
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if("["+JSON.stringify(this.authService.currentUser$.value._id)+"]" === JSON.stringify(this.newMatch.matchMadeBy)){
        console.log(' allowed');
        this.deleteMatch();
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log("["+JSON.stringify(this.authService.currentUser$.value._id)+"]");
        this.router.navigateByUrl('auth/login');
      }
    }
  }

  createComment(){
    console.log('create comment called');
    this.router.navigate(['comments/create/' + this.newMatch._id]);
  }

  
}
