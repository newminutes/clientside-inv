import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'Hiermee kan een gebruiker registreren',
      scenario: [
        'Gebruiker vult email in, naam en passwoord.',
        'De applicatie valideerd de ingevoerde gegevens',
        'Indien de gegevens correct zijn word er automatisch ingelogd in de applicatie en wordt er genavigeerd naar het startscherm.'],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is geregistreerd en ingelogt.',
    },
    {
      id: 'UC-03',
      name: 'Wedstrijd aanmaken',
      description: 'Hiermee kan de gebruiker een Brazilian jiu jitsu wedstrijd aanmaken.',
      scenario: [
        'gebruiker vult naam, land en de locatie van een wedstrijd in.',
        'De applicatie valideerd de ingevoerde gegevens',
        'Indien de gegevens correct zijn word er een wedstrijd aangemaakt en zal de applicatie automatisch redirecte naar de hoofdpagina.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is geregistreerd en ingelogt.',
      postcondition: 'De wedstrijd is aangemaakt.',
    },
    {
      id: 'UC-04',
      name: 'Wedstrijd aanpassen',
      description: 'Hiermee kan de gebruiker een opgezette Brazilian jiu jitsu competitie aanpassen.',
      scenario: [
        'gebruiker klikt op wedstrijd.',
        'gebruiker klikt op aanpassen.',
        'gebruiker vult naam, land en de locatie van een wedstrijd in.',
        'De applicatie valideerd de ingevoerde gegevens',
        'Indien de gegevens correct zijn word er een wedstrijd aangemaakt en zal de applicatie automatisch redirecte naar de hoofdpagina.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is geregistreerd en ingelogt.',
      postcondition: 'De wedstrijd is aangepast.',
    },
    {
      id: 'UC-05',
      name: 'Wedstrijd verwijderen',
      description: 'Hiermee kan de gebruiker een bestaande Brazilian jiu jitsu competitie verwijderen.',
      scenario: [
        'gebruiker klikt op wedstrijd.',
        'gebruiker klikt op wedstrijd verwijderen.',
        ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is geregistreerd en ingelogt.',
      postcondition: 'De wedstrijd is verwijderd.',
    },
    {
      id: 'UC-06',
      name: 'Comment plaatsen over wedstrijd',
      description: 'Hier kan een gebruiker commentaar geven op een aangemaakte wedstrijd.',
      scenario: [
        'Gebruiker vult commentaar text in.',
        'De applicatie valideerd de ingevoerde gegevens',
        'Indien de gegevens correct zijn word er commentaar aangemaakt en zal de applicatie automatisch redirecte naar de detail pagina van de wedstrijd.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is geregistreerd en ingelogt.',
      postcondition: 'Het commentaar is aangemaakt.',
    },
    {
      id: 'UC-07',
      name: 'Comment aanpassen over wedstrijd',
      description: 'Hier kan een gebruiker commentaar aanpassen voor gemaakte commentaar van zichzelf.',
      scenario: [
        'Gebruiker klikt op commentaar.',
        'Gebruiker klikt op commentaar aanpassen.',
        'Gebruiker vult commentaar text in.',
        'De applicatie valideerd de ingevoerde gegevens',
        'Indien de gegevens correct zijn word er commentaar aangemaakt en zal de applicatie automatisch redirecte naar de detail pagina van de wedstrijd.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is geregistreerd en ingelogt.',
      postcondition: 'Het commentaar is aangepast.',
    },
    {
      id: 'UC-08',
      name: 'Comment verwijderen over wedstrijd',
      description: 'Hier kan een gebruiker commentaar verwijderen voor gemaakte commentaar van zichzelf.',
      scenario: [
        'Gebruiker klikt op commentaar.',
        'Gebruiker klikt op commentaar verwijderen.',
        ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is geregistreerd en ingelogt.',
      postcondition: 'Het commentaar is verwijderd.',
    },
    {
      id: 'UC-09',
      name: 'Wedstrijd in mand stoppen',
      description: 'Hier kan een gebruiker een wedstrijd waarvoor hij/zij interesse heeft deze in een mand plaatsen.',
      scenario: [
        'Selecteerd wedstrijd',
        'Klikt op wedstrijd in mand stoppen',
        'Gaat naar de mand waar de wedstrijden inzitten.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is geregistreerd en ingelogt.',
      postcondition: 'Wedstrijd is in mand gestopt.',
    },
    {
      id: 'UC-010',
      name: 'Wedstrijd uit mand halen',
      description: 'Hier kan een gebruiker de wedstrijd uit de mand halen.',
      scenario: [
        'Selecteerd wedstrijd',
        'Klikt op verwijderen uit mand',
        'Gaat naar de mand waar de wedstrijden inzitten.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is geregistreerd en ingelogt.',
      postcondition: 'Wedstrijd is uit mand gehaald.',
    },
    {
      id: 'UC-011',
      name: 'Wedstrijden zien in lijst',
      description: 'Hier kan een iedereen alle wedstrijden zien',
      scenario: [
        'weergeeft lijst met wedstrijden.',
        ],
      actor: this.PLAIN_USER,
      precondition: 'Geen.',
      postcondition: 'ziet lijst met wedstrijden',
    },
    {
      id: 'UC-012',
      name: 'Commentaar zien van wedstrijden',
      description: 'Hier kan iedereen op een wedstrijd klikken en al het commentaar bekijken',
      scenario: [
        'weergeeft wedstrijd met lijst van alle commentaar.',
        ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'Ziet commentaar van wedstrijden op pagina.',
    },
    {
      id: 'UC-013',
      name: 'mand met wedstrijden bekijken',
      description: 'Hier kan een gebruiker alle wedstrijden van de mand zien',
      scenario: [
        'weergeeft wedstrijden.',
        ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is geregistreerd en ingelogt.',
      postcondition: 'Ziet wedstrijden.',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
