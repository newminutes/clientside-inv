import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsecasesComponent } from './usecases.component';
import { UsecaseComponent } from './usecase/usecase.component';

describe('UsecaseComponent', () => {
  let component: UsecasesComponent;
  let fixture: ComponentFixture<UsecasesComponent>;

  
  beforeEach( () => {
    TestBed.configureTestingModule({
     declarations: [ UsecasesComponent ]
   })
   .compileComponents();
    fixture = TestBed.createComponent(UsecasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
 });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
