import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { Comment } from './comment.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  getById(id: string): Observable<any> {
    return this.http.get<any>(environment.apiUrl + `comment/${id}`);
  }

  deleteComment(comment: Comment): Observable<any> {
    console.log('delete called');
    return this.http.delete<any>(environment.apiUrl + `comment/${comment._id}`);
  }

  updateComment(id: string, comment: Comment): Observable<any> {
    const headers = { 'content-type': 'application/json'};
    return this.http.put<any>(environment.apiUrl + `comment/${id}`, comment, {headers});
  }
}
