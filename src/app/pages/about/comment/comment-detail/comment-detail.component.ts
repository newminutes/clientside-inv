import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommentService } from '../comment.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AuthService } from '../../../../auth/auth-service';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Comment } from '../comment.model';
import { LoggedInAuthGuard } from '../../../../auth/logged-in-auth-guard';
import { MatchService } from '../../../match/match.service';
import { Match } from '../../../match/match.model';

@Component({
  selector: 'app-comment-detail',
  templateUrl: './comment-detail.component.html',
  styleUrls: ['./comment-detail.component.css']
})
export class CommentDetailComponent implements OnInit {


  comment$: Observable<any>;
  comment = new Comment();
  subscription: Subscription;
  id = '';
  private routeSub: Subscription;

  matches: Match[] = [];
  getAllMatchSubscription: Subscription;

  constructor(private commentService: CommentService, private matchService: MatchService, private route: ActivatedRoute,
              public authService: AuthService, private router: Router, public logged : LoggedInAuthGuard ) { }
 

  ngOnInit(): void {
    this.getAll()
    this.comment$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap)=>
      this.commentService.getById(params.get('id'))
      )
    );


    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });


    this.subscription = this.commentService.getById(this.id)
    .subscribe(data => {
      console.log(data);
      this.comment = data;
    });

   
  }

  getAll(){
    this.getAllMatchSubscription = this.matchService.getAll().subscribe(matches => {
      this.matches = matches
    });
   }

  deleteComment(){
    console.log('name: ', this.comment.title)
    this.commentService.deleteComment(this.comment).subscribe(data=>{
      console.log(data);
      //this.router.navigateByUrl('matches');
    });
    this.deleteCommentInArray(this.id)
  }

  deleteCommentInArray(commentId){
    this.matches.forEach(match => {
      console.log('THIS IS DE MATCH: ', match.comments)
      match.comments?.forEach(comment => {
        console.log('ERIN ')
        if(commentId == comment){
          const index = match.comments.indexOf(comment);
          match.comments.splice(index, 1);
          console.log('Belongs to match: ', match);
    this.matchService.updateMatch(match._id, match)
    .subscribe(data => {
      this.router.navigate([`/matches/` +match._id]);
    })
        }

      });
    });
  }

 
  

  tryToEdit(){
    console.log('edit called')
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if(JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(this.comment.user)){
        console.log(' allowed');
        this.router.navigate(['comments/' + this.comment._id + '/edit']);
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log("["+JSON.stringify(this.authService.currentUser$.value._id)+"]");
        this.router.navigateByUrl('auth/login');
      }
    }
  }

  tryToDelete(){
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if(JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(this.comment.user)){
        console.log(' allowed');
        this.deleteComment();
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log("["+JSON.stringify(this.authService.currentUser$.value._id)+"]");
        this.router.navigateByUrl('auth/login');
      }
    }
  }
  
 
  
}
