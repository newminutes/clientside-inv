import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentDetailComponent } from './comment-detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CommentDetailComponent', () => {
  let component: CommentDetailComponent;
  let fixture: ComponentFixture<CommentDetailComponent>;

 
  beforeEach( () => {
    TestBed.configureTestingModule({
     imports: [RouterTestingModule, HttpClientTestingModule ],
     declarations: [ CommentDetailComponent ]
   })
   .compileComponents();
    fixture = TestBed.createComponent(CommentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
 });


  

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
