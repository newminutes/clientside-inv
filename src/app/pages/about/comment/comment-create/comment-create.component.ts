import { Component, OnInit } from '@angular/core';
import { MatchService } from '../../../match/match.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AuthService } from '../../../../auth/auth-service';
import { Match } from '../../../match/match.model';
import { Subscription, Observable } from 'rxjs';
import { Comment } from '../comment.model';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-comment-create',
  templateUrl: './comment-create.component.html',
  styleUrls: ['./comment-create.component.css']
})
export class CommentCreateComponent implements OnInit {

  constructor(private matchService: MatchService, private router: Router, public authService: AuthService, private route: ActivatedRoute) { }

  id = this.route.snapshot.params.id;
  comment$: Observable<any>;
  private routeSub: Subscription;
  subscription: Subscription;

  match = new Match();

  public selectedValue: string;
  public selectedMatch: Match;
  createCommentSubscription: Subscription;
  getAllMatchesSubscription: Subscription;
  matches$: Observable<Match[]>;

  comment = new Comment();

  ngOnInit(): void {
    console.log('matches geladen');

    this.comment$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap)=>
      this.matchService.getCommentById(params.get('id'))
      )
    );


    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    this.matchService.getById(this.id)
    .subscribe(data => {
      this.match = data;
    })


  }

  createComment() {
    console.log('create comment called');

    // this.match.firstFighter = this.selectedFirstFighter;
    // this.match.secondFighter = this.selectedSecondFighter;


    this.comment.user = this.authService.currentUser$.value._id;
    this.comment.userName = this.authService.currentUser$.value.email;
    this.createCommentSubscription = this.matchService.createComment(this.comment)
    .subscribe(async data => {
      this.comment = await data;
      console.log(data);
     // this.router.navigate(['/matches']);
     this.putOnMatch(data.id)
    });
  }

  putOnMatch(commentId){
    this.match.comments.push(commentId)
    this.matchService.updateMatch(this.id, this.match)
    .subscribe(data => {
      this.router.navigate([`/matches/` + this.id]);
    })
  }

}
