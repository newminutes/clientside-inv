import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommentService } from '../comment.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AuthService } from '../../../../auth/auth-service';
import { switchMap } from 'rxjs/operators';
import { Comment } from '../comment.model';
import { Match } from '../../../match/match.model';
import { MatchService } from '../../../match/match.service';

@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.css']
})
export class CommentEditComponent implements OnInit {

  matches: Match[] = [];
  getAllMatchSubscription: Subscription;

  subscription: Subscription;


  public selectedValue: string;
 

  public commentPreDefined = new Comment();

  id = this.route.snapshot.params.id;
  comment = new Comment();
  subscriptionoptions: Subscription;
  subscriptionParams: Subscription;
  constructor(private commentService: CommentService, private route: ActivatedRoute,
    private router: Router,
    public authService: AuthService, private matchService: MatchService) { }

  ngOnInit(): void {
    console.log('detail page called')
    this.getAll()

    this.subscription = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.commentService.getById(params.get('id'));
      })
    ).pipe(
    ).subscribe(data => {
      this.comment = data;
    })
  }

  getAll(){
    this.getAllMatchSubscription = this.matchService.getAll().subscribe(matches => {
      this.matches = matches
    });
   }

  updateComment(){
    this.comment.user = this.authService.currentUser$.value._id;
    this.subscriptionParams = this.commentService.updateComment(this.id, this.comment)
    .subscribe(data => {
      console.log(data);
      //this.router.navigate(['/comments/', this.id]);
    });
    this.updateCommentInArray(this.id)
  }

  updateCommentInArray(commentId){
    this.matches.forEach(match => {
      console.log('THIS IS DE MATCH: ', match.comments)
      match.comments?.forEach(comment => {
        console.log('ERIN ')
        if(commentId == comment){
          const index = match.comments.indexOf(comment);
          //match.comments.splice(index, 1); update zijn
          match.comments[index] = this.comment
          console.log('Belongs to match: ', match);
    this.matchService.updateMatch(match._id, match)
    .subscribe(data => {
      this.router.navigate([`/matches/` +match._id]);
    })
        }

      });
    });
  }


}
