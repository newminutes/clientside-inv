import { User } from '../../user/user.model';

export class Comment {
    _id: string;
    text: string;
    title: string;
    userName: string;
    user?: string;
}
