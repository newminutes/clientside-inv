import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CommentEditComponent } from './comment-edit/comment-edit.component';
import { CommentCreateComponent } from './comment-create/comment-create.component';
import { CommentDetailComponent } from './comment-detail/comment-detail.component';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { LoggedInAuthGuard } from '../../../auth/logged-in-auth-guard';


const routes: Routes = [
  {
    path: ':id/edit', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: CommentEditComponent,
  },
  {
    path: 'create/:id', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: CommentCreateComponent,
  },
  { path: '', pathMatch: 'full', component: CommentCreateComponent },
  {
    path: ':id', pathMatch: 'full', component: CommentDetailComponent,
  },


];

@NgModule({
  declarations: [CommentEditComponent, CommentCreateComponent, CommentDetailComponent],
  imports: [
    CommonModule,
    FormsModule ,
    MatFormFieldModule, MatSelectModule,
    RouterModule.forChild(routes)
  ]
})
export class CommentModule { }
