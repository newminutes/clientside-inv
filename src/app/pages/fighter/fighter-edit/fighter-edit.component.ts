import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FighterService } from '../fighter.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AuthService } from '../../../auth/auth-service';
import { Fighter } from '../fighter.model';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-fighter-edit',
  templateUrl: './fighter-edit.component.html',
  styleUrls: ['./fighter-edit.component.css']
})
export class FighterEditComponent implements OnInit {
  subscription: Subscription;


  public selectedValue: string;
 

  public fighterPreDefined = new Fighter();

  // id = this.route.snapshot.params.id;
  fighter = new Fighter();
  subscriptionoptions: Subscription;
  subscriptionParams: Subscription;

  constructor(private fighterService: FighterService, private route: ActivatedRoute,
    private router: Router,
    public authService: AuthService) { }
  

  ngOnInit(): void {
    console.log('detail page called')

    this.subscription = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.fighterService.getById(params.get('id'));
      })
    ).subscribe(data => {
      this.fighter = data;
    }, error => {
      
    })
  }


  updateFighter(){
    this.fighter.fighterMadeBy = this.authService.currentUser$.value._id;
    this.subscriptionParams = this.fighterService.updateFighter(this.fighter._id, this.fighter)
    .subscribe(data => {
      console.log(data);
      this.router.navigate(['/fighters']);
    });
    alert("Fighter is edited")
  }


}
