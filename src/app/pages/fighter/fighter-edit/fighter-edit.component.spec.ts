import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FighterEditComponent } from './fighter-edit.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { User } from '../../user/user.model';
import { Fighter } from '../fighter.model';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { AuthService } from '../../../auth/auth-service';
import { Router, ActivatedRoute, convertToParamMap } from '@angular/router';
import { FighterService } from '../fighter.service';


const expectedFighter: Fighter = {
    _id: '5',
    fightstyle: 'pressure',
    name: 'mendez',
    sex: 'male',
};

const fighters: Fighter[] = [{ _id: '5',
fightstyle: 'pressure',
name: 'mendez',
sex: 'male', },
 {_id: '6',
fightstyle: 'pressure',
name: 'rafa',
sex: 'male',
}] as Fighter[];
 



const updateFighter: Fighter = {
    _id: '5',
    fightstyle: 'newfightstyle',
    name: 'eeeee',
    sex: 'Male',
};



const expectedUserData: User = {
  _id: '1',
  name: 'afrash',
  email: 'user@host.com',
  password: 'test',
  accessToken: 'some.dummy.token',
};


describe('FighterEditComponent', () => {
  let component: FighterEditComponent;
  let fixture: ComponentFixture<FighterEditComponent>;
  
  let fighterServiceSpy;
  let authServiceSpy;
  let routerSpy;

  beforeEach(() => {
   
    authServiceSpy = jasmine.createSpyObj(
      'AuthService',
      [
        'login',
        'register',
        'logout',
        'getUserFromLocalStorage',
        'saveUserToLocalStorage',
        'userMayEdit',
      ]
      // ['currentUser$']
    );
    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authServiceSpy.currentUser$ = mockUser$;
    
    fighterServiceSpy = jasmine.createSpyObj('FighterService', ['createFighter', 'getAll', 'getById', 'updateFighter', 'deleteFighter']);
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

    TestBed.configureTestingModule({
      // The declared components needed to test the UsersComponent.
      declarations: [
        FighterEditComponent, // The 'real' component that we will test
       
      ],
      imports: [FormsModule, RouterTestingModule, HttpClientModule],
      //
      // The constructor of our real component uses dependency injected services
      // Never provide the real service in testcases!
      //
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        // { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: FighterService, useValue: fighterServiceSpy },
        // { provide: FighterService, useValue: fighterServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {paramMap: of(convertToParamMap({id: '1'}))}
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(FighterEditComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be able to edit an match', () => {
    authServiceSpy.getUserFromLocalStorage.and.returnValue(of(expectedUserData));
    component.fighter = expectedFighter;
    component.subscription = new Subscription();
    console.log(expectedFighter);
    expect(component.fighter).toEqual(expectedFighter);
    expect(authServiceSpy.currentUser$.value).toEqual(expectedUserData);
    component.fighter.name = 'test2';
    expect(component.fighter).toEqual(expectedFighter);
    // console.log(component.artist)
    fighterServiceSpy.updateFighter.withArgs(component.fighter._id, component.fighter).and.returnValue(of(updateFighter));
    console.log(component.fighter);
    expect(component.fighter).toEqual(expectedFighter);
  });

  it('should have an fighter and is logged on', (done) => {
    // artistServiceSpy.getArtistById.and.returnValue(of(expectedUser));
    authServiceSpy.getUserFromLocalStorage.and.returnValue(of(expectedUserData));
    component.fighter = expectedFighter;
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.fighter).toEqual(expectedFighter);
    setTimeout(() => {
      expect(authServiceSpy.currentUser$.getValue()).toEqual(expectedUserData);
      done();
    }, 200);
  });

  it('should have an fighter and is logged on, edit succesfull', (done) => {
    // artistServiceSpy.getArtistById.and.returnValue(of(expectedUser));
    fighterServiceSpy.getById.withArgs('5').and.returnValue(of(expectedFighter));
    fighterServiceSpy.updateFighter.withArgs(expectedFighter._id, expectedFighter).and.returnValue(of(updateFighter));
    authServiceSpy.getUserFromLocalStorage.and.returnValue(of(expectedUserData));

    component.subscription = fighterServiceSpy.getById('5').subscribe(data => {
      component.fighter = data;
    });

    fixture.detectChanges();
    expect(component.fighter).toEqual(expectedFighter);
    expect(authServiceSpy.currentUser$.value).toEqual(expectedUserData);

    component.fighter.name = 'eeeee';
    component.fighter.fightstyle = 'newfightstyle';
    component.fighter.sex = 'Male';

    fixture.detectChanges();

    component.subscription = fighterServiceSpy.updateFighter(component.fighter._id, component.fighter).subscribe(fighter => {
      component.fighter = fighter;
    });

    fixture.detectChanges();
    setTimeout(() => {
      expect(component.fighter).toEqual(updateFighter);
      expect(expectedFighter).toEqual(updateFighter);
      done();
    }, 200);
  });


  it('Fighter does not exist', () => {
    fighterServiceSpy.getById.withArgs('500').and.returnValue(of(undefined));
    authServiceSpy.getUserFromLocalStorage.and.returnValue(of(expectedUserData));



    component.subscription = fighterServiceSpy.getById('500').subscribe(data => {
      component.fighter = data;
    });

    fixture.detectChanges();
    expect(component.fighter).toBeFalsy();
      });

});
