import { TestBed } from '@angular/core/testing';

import { FighterService } from './fighter.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { Fighter } from './fighter.model';
import { of } from 'rxjs';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('FighterService', () => {
  let service: FighterService;

  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule],
    });
    service = TestBed.inject(FighterService);
    httpMock = TestBed.inject(HttpTestingController);
  });

 

  //   //service = new FighterService(httpSpy)
  // });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should get all fighters', () => {
    // Set input and expected output
    const dummyFighters: Fighter[] = [
      {_id: '1', fightstyle: 'pressure', name: 'gui mendez', sex: 'male'},
      {_id: '2', fightstyle: 'pressure', name: 'rafa mendez', sex: 'male'}
    ] as Fighter[];

    // Mock functions that are called on the way
    service.getAll().subscribe(fighters => {
      expect(fighters.length).toBe(2);
      expect(fighters).toEqual(dummyFighters);
    });
    const request = httpMock.expectOne(environment.apiUrl + 'fighter');
    expect(request.request.method).toBe('GET');
    request.flush(dummyFighters);

  });

  
  it('should return fighter by Id', () => {
    // Set input and expected output
    const dummyFighters: Fighter[] = [
      {_id: '1', fightstyle: 'pressure', name: 'gui mendez', sex: 'male'},
      {_id: '2', fightstyle: 'pressure', name: 'rafa mendez', sex: 'male'}
    ] as Fighter[];

    // Mock functions that are called on the way
    service.getById('1').subscribe(fighters => {
      expect(fighters[0]).toEqual(dummyFighters[0]);
    });
    const request = httpMock.expectOne(environment.apiUrl + `fighter/1`);
    expect(request.request.method).toBe('GET');
    request.flush(dummyFighters);

  });

  it('should not return fighter by Id', () => {
    // Set input and expected output
    const dummyFighters: Fighter[] = [
      {_id: '1', fightstyle: 'pressure', name: 'gui mendez', sex: 'male'},
      {_id: '2', fightstyle: 'pressure', name: 'rafa mendez', sex: 'male'}
    ] as Fighter[];

    // Mock functions that are called on the way
    service.getById('6').subscribe(fighters => {
      expect(fighters.length).toBe(0);
    });
    const request = httpMock.expectNone(environment.apiUrl + `fighter/1`);
    
    

  });

  it('should not return fighter by Id', () => {
    
   service.getAll().subscribe(fighters => {
    expect(fighters.length).toBe(0);
  });

  
  });

  it('should update a Fighter', () => {
    // Set input and expected output

    var fighter: Fighter =  { _id: '1', fightstyle: 'aaaa',  name: 'ff', sex: 'ff'};


    
    // Mock functions that are called on the way
    service.updateFighter(fighter._id, fighter).subscribe(fighter => {
      expect(fighter._id).toEqual('1');
    });
    const request = httpMock.expectOne(environment.apiUrl + `fighter/1`);
    expect(request.request.method).toBe('PUT');
  });



  it('should create fighter', () => {
    // Set input and expected output
    
    var fighter: Fighter =  { _id: '1', fightstyle: 'aaaa',  name: 'ff', sex: 'ff'};
    
    // Mock functions that are called on the way
    service.createFighter(fighter).subscribe(fighter => {
      expect(fighter._id).toEqual('1');
    });
    const request = httpMock.expectOne(environment.apiUrl + `fighter`);
    expect(request.request.method).toBe('POST');
  });

  it('should not create fighter', () => {
    // Set input and expected output
    
    var fighter: Fighter =  { _id: '', fightstyle: 'aaaa',  name: 'ff', sex: 'ff'};
    
    // Mock functions that are called on the way
    service.createFighter(fighter).subscribe(fighter => {
      expect(fighter._id).toBeUndefined('1');
    });
    const request = httpMock.expectOne(environment.apiUrl + `fighter`);
    expect(request.request.method).toBe('POST');
  });

  
})
