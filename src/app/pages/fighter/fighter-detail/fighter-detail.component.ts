import { Component, OnInit, OnDestroy } from '@angular/core';
import { FighterService } from '../fighter.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AuthService } from '../../../auth/auth-service';
import { Subscription, Observable } from 'rxjs';
import { Fighter } from '../fighter.model';
import { switchMap } from 'rxjs/operators';
import { LoggedInAuthGuard } from '../../../auth/logged-in-auth-guard';
import { User } from '../../user/user.model';
import { UserService } from '../../user/user.service';

@Component({
  selector: 'app-fighter-detail',
  templateUrl: './fighter-detail.component.html',
  styleUrls: ['./fighter-detail.component.css']
})
export class FighterDetailComponent implements OnInit, OnDestroy {


  fighter$: Observable<any>;
  fighter = new Fighter();
  user = new User();
  subscription: Subscription;
  id = '';
  userSub: Subscription;
  private routeSub: Subscription;

  constructor(private fighterService: FighterService, private route: ActivatedRoute,
              public authService: AuthService, private router: Router, public logged : LoggedInAuthGuard, private userService: UserService) { }


  ngOnInit(): void {
    console.log('detail page called');

    this.fighter$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap)=>
      this.fighterService.getById(params.get('id'))
      )
    );


    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });


    this.subscription = this.fighterService.getById(this.id)
    .subscribe(data => {
      console.log(data);
      this.fighter = data;
      this.getUser(this.fighter.fighterMadeBy[0]);
    });
  }

  getUser(id){
    this.userService.getById(id).subscribe(data=>{
      console.log('DATA, ' + data.email)
      this.user = data;
    })
  }

  deleteFighter(){
    console.log('name: ', this.fighter.name)
    this.fighterService.deleteFighter(this.fighter).subscribe(data=>{
      console.log(data);
      this.router.navigateByUrl('fighters');
    });
  }

  

  tryToEdit(){
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if("["+JSON.stringify(this.authService.currentUser$.value._id)+"]" === JSON.stringify(this.fighter.fighterMadeBy)){
        console.log(' allowed');
        this.router.navigateByUrl('fighters/' + this.fighter._id + '/edit' );
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log("["+JSON.stringify(this.authService.currentUser$.value._id)+"]");
        this.router.navigateByUrl('auth/login');
      }
    }
  }

  tryToDelete(){
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if("["+JSON.stringify(this.authService.currentUser$.value._id)+"]" === JSON.stringify(this.fighter.fighterMadeBy)){
        console.log(' allowed');
        this.deleteFighter();
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log("["+JSON.stringify(this.authService.currentUser$.value._id)+"]");
        this.router.navigateByUrl('auth/login');
      }
    }
  }
  

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
    if(this.routeSub){
      this.routeSub.unsubscribe();
    }
  }


}



