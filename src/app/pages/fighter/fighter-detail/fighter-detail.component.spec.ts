import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FighterDetailComponent } from './fighter-detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FighterDetailComponent', () => {
  let component: FighterDetailComponent;
  let fixture: ComponentFixture<FighterDetailComponent>;
  

  
  beforeEach( () => {
    TestBed.configureTestingModule({
     imports: [RouterTestingModule, HttpClientTestingModule ],
     declarations: [ FighterDetailComponent ]
   })
   .compileComponents();
    fixture = TestBed.createComponent(FighterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
 });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
