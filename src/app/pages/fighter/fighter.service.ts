import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Fighter } from './fighter.model';

@Injectable({
  providedIn: 'root'
})
export class FighterService {

  

  constructor(private http: HttpClient) { }

  getAll(): Observable<any[]> {
    return this.http.get<any>(environment.apiUrl + 'fighter');
  }

  getById(id: string): Observable<any> {
    return this.http.get<any>(environment.apiUrl + `fighter/${id}`);
  }

  createFighter(fighter: Fighter): Observable<any> {
    const headers = { 'content-type': 'application/json'}
    return this.http.post<any>(environment.apiUrl + 'fighter', fighter, {headers});
  }

  updateFighter(id: string, fighter: Fighter): Observable<any> {
    const headers = { 'content-type': 'application/json'};
    return this.http.put<any>(environment.apiUrl + `fighter/${id}`, fighter, {headers});
  }

  deleteFighter(fighter: Fighter): Observable<any> {
    console.log('delete called');
    return this.http.delete<any>(environment.apiUrl + `fighter/${fighter._id}`);
  }

}
