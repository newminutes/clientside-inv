import { Component, OnInit } from '@angular/core';
import { FighterService } from '../fighter.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../auth/auth-service';
import { Fighter } from '../fighter.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fighter-create',
  templateUrl: './fighter-create.component.html',
  styleUrls: ['./fighter-create.component.css']
})
export class FighterCreateComponent implements OnInit {

  constructor(private fighterService: FighterService, private router: Router, public authService: AuthService) { }

  public selectedValue: string;
  //public fighter: Fighter;
  createFighterSubscription: Subscription;

  fighter = new Fighter();

  ngOnInit(): void {
    console.log('fighters geladen');

  }

  createFighter() {
    console.log('create fighter called');
    this.fighter.fighterMadeBy = this.authService.currentUser$.value._id;
    this.createFighterSubscription = this.fighterService.createFighter(this.fighter)
    .subscribe(data => {
      console.log(data);
      this.router.navigate(['/fighters']);
    });

  }

 

   ngOnDestroy(): void {
    if(this.createFighterSubscription){
      this.createFighterSubscription.unsubscribe();
    }
    
  }

}
