import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FighterCreateComponent } from './fighter-create.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';

describe('FighterCreateComponent', () => {
  let component: FighterCreateComponent;
  let fixture: ComponentFixture<FighterCreateComponent>;




  beforeEach( () => {
    TestBed.configureTestingModule({
     imports: [RouterTestingModule, HttpClientTestingModule ,FormsModule],
     declarations: [ FighterCreateComponent ]
   })
   .compileComponents();
    fixture = TestBed.createComponent(FighterCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
 });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
