import { Match } from '../match/match.model';
export class Fighter {
    _id: string;
    fightstyle: string;
    name: string;
    sex: string;
    matches?: Match[];
    fighterMadeBy?: string;
}
