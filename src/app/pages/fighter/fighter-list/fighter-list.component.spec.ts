import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FighterListComponent } from './fighter-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FighterEditComponent } from '../fighter-edit/fighter-edit.component';

describe('FighterListComponent', () => {
  let component: FighterListComponent;
  let fixture: ComponentFixture<FighterListComponent>;
  
  
  let fightServiceSpy;
  let authServiceSpy;
  let routerSpy;
  
  beforeEach( () => {
    TestBed.configureTestingModule({
     imports: [RouterTestingModule, HttpClientTestingModule ],
     declarations: [ FighterListComponent ]
   })
   .compileComponents();
    fixture = TestBed.createComponent(FighterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
 });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
