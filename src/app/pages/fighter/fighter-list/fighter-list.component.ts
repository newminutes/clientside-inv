import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { FighterService } from '../fighter.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../auth/auth-service';
import { Fighter } from '../fighter.model';

@Component({
  selector: 'app-fighter-list',
  templateUrl: './fighter-list.component.html',
  styleUrls: ['./fighter-list.component.css']
})
export class FighterListComponent implements OnInit, OnDestroy {

  fighters$: Observable<any[]>;
  subscription: Subscription;


  fighter = new Observable<any>();
  constructor(private fighterService: FighterService, private route: ActivatedRoute,
              public authService: AuthService) { }
 

  ngOnInit(): void {
    console.log('Matches geladen');

    this.subscription = this.fighterService.getAll()
    .subscribe(fighter => {
      this.fighters$ = this.fighterService.getAll();
    });

  }


  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
