import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FighterEditComponent } from './fighter-edit/fighter-edit.component';
import { FighterCreateComponent } from './fighter-create/fighter-create.component';
import { FighterListComponent } from './fighter-list/fighter-list.component';
import { FighterDetailComponent } from './fighter-detail/fighter-detail.component';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { LoggedInAuthGuard } from '../../auth/logged-in-auth-guard';

const routes: Routes = [
  {
    path: ':id/edit', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: FighterEditComponent,
  },
  {
    path: 'create', pathMatch: 'full', canActivate: [LoggedInAuthGuard],  component: FighterCreateComponent,
  },
  { path: '', pathMatch: 'full', component: FighterListComponent },
  {
    path: ':id', pathMatch: 'full', component: FighterDetailComponent,
  },


];

@NgModule({
  declarations: [FighterListComponent, FighterEditComponent, FighterCreateComponent, FighterDetailComponent],
  imports: [
    CommonModule,
    FormsModule ,
    MatFormFieldModule, MatSelectModule,
    RouterModule.forChild(routes)
  ]
})
export class FighterModule { }
