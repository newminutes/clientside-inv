import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { LayoutComponent } from '../app/layout/layout/layout.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { MatchListComponent } from './pages/match/match-list/match-list.component';
import { AuthenticationModule } from './auth/authentication.module';
import { CommonModule } from '@angular/common';
import { FighterModule } from './pages/fighter/fighter.module';
import { CommentModule } from './pages/about/comment/comment.module';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'auth/login' },
      { path: 'dashboard', pathMatch: 'full', component: DashboardComponent },
      { path: 'about', pathMatch: 'full', component: UsecasesComponent },
      // { path: 'matchList', pathMatch: 'full', component: MatchListComponent },
      {
        path: 'fighters',
        loadChildren: () =>
          import('../app/pages/fighter/fighter.module').then((m) => m.FighterModule),
      },
      {
        path: 'matches',
        loadChildren: () =>
          import('../app/pages/match/match.module').then((m) => m.MatchModule),
      },
      {
        path: 'users',
        loadChildren: () =>
          import('./pages/user/user.module').then((m) => m.UserModule),
      },
      {
        path: 'comments',
        loadChildren: () =>
          import('./pages/about/comment/comment.module').then((m) => m.CommentModule),
      },
      {
        path: 'auth',
        loadChildren: () =>
          import('./auth/authentication.module').then((m) => m.AuthenticationModule),
      },
    ],
  },
  // { path: 'login', pathMatch: 'full', component: LoginComponent },
  // { path: 'login', pathMatch: 'full', component: LoginComponent },
  
  // { path: 'register', pathMatch: 'full', component: RegisterComponent },
  // { path: 'register', pathMatch: 'full', component: RegisterComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
