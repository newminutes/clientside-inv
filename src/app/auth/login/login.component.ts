import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from '../auth-service';
import { Login } from '../login.model';
import { Auth as User } from '../auth.model';
import { AlertService } from '../../alert/alert.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  loginForm: FormGroup;
  returnUrl: string;
  public newUser = new User();

  email: string;
  password: string;

  constructor(private router: Router, private authService: AuthService, private alertService: AlertService,
              private route: ActivatedRoute,) {}


  public currentUser$ = new BehaviorSubject<User>(undefined);

  private readonly CURRENT_USER = 'currentuser';

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  

  ngOnInit(): void {
    this.email = "";
    this.password = "";
  }

  errorThrown = false;

  onSubmit(){
  // if(this.loginForm.valid) {
    // const email = this.loginForm.value.email;
    // const password = this.loginForm.value.password;
    this.subscription = this.authService.login(this.email, this.password).subscribe((user) => {
      if(user !== undefined) {
        console.log('user = ', user);
        this.router.navigate(['/matches']);
        alert("You are logged in")
      }else{
        console.log('error thrown');
        this.errorThrown = true;
      }
    });

  // } else {
  //   console.error('loginForm invalid');
  
  // }
}

ngOnDestroy(){
  
  if(this.subscription){
    this.subscription.unsubscribe();
  }

}


}
