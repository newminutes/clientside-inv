import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginComponent } from './login.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

   
  beforeEach( () => {
    TestBed.configureTestingModule({
     imports: [RouterTestingModule, HttpClientTestingModule, FormsModule ],
     declarations: [ LoginComponent ]
   })
   .compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
 });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
//