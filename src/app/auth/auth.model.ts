export class Auth {
    id: number;
    name: string;
    email: string;
    password: string;
    role?: string;
    accessToken?: string;
    applied?: [];
}
