import { TestBed } from '@angular/core/testing';

import { LoggedInAuthGuard } from './logged-in-auth-guard';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LoggedInAuthGuardGuard', () => {
  let guard: LoggedInAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [ RouterTestingModule, HttpClientTestingModule ]
    });
    guard = TestBed.inject(LoggedInAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
