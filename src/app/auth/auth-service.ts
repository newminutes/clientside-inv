import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../pages/user/user.model';
import { Auth } from './auth.model';
import { Login } from './login.model';
import { AlertService } from '../alert/alert.service';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private alertService: AlertService, private router: Router) { }


  public currentUser$ = new BehaviorSubject<User>(undefined);

  private readonly CURRENT_USER = 'currentuser';

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json',
  });

 register(user: User): Observable<any> {
    const headers = { 'content-type': 'application/json'};
    return this.http.post<any>(environment.apiUrl + 'user/signup', user, {headers});
  }

  login(email: string, password: string): Observable<User> {
    // const headers = { 'content-type': 'application/json'};
    return this.http.post<any>(environment.apiUrl + 'user/login', {email, password}, {headers: this.headers}
    )
    .pipe(
      map((response: any) => {
        const user = { ...response} as User;
        this.saveUserToLocalStorage(user);
        this.currentUser$.next(user);
        console.log('You have been logged in');
        this.alertService.success('You have been logged in');
        console.log(this.currentUser$.value);
        return user;
      }),
      catchError((error: any) => {
        console.log('error', error);
        console.log('error.message', error.message);
        console.log('error.error.message', error.error.message);
        this.alertService.error(error.error.message || error.message);
        return of(undefined);
      })
    );
  }

  userMayEdit(itemUserId: string): Observable<boolean> {
    console.log('User may edit is called')
    return this.currentUser$.pipe(
      map((user: User) => (user? user._id === itemUserId : false))
    );
  }

  

  logout() {
    // remove user from local storage to log user out
    console.log('logout called');
    // localStorage.removeItem('currentUser');
    const removeToken = localStorage.removeItem('currentuser');
    if (removeToken == null) {
      this.router.navigate(['register/login']);
    }

    localStorage.clear();
    this.currentUser$.next(undefined);
    alert("You have been logged out.")
    // this.alertService.success('You have been logged out.')
    // this.router.navigateByUrl('register/login');
}

  getUserFromLocalStorage(): Observable<User> {
    const localUser = JSON.parse(localStorage.getItem(this.CURRENT_USER));
    return of(localUser);
  }

  private saveUserToLocalStorage(user: User): void {
    localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('currentuser');
    return (authToken !== null) ? true : false;
  }

}
