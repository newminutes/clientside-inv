import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../pages/user/user.model';
import { AuthService } from '../auth-service';
import { Router } from '@angular/router';
import { Auth } from '../auth.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
user = new User();
subscription: Subscription;

  constructor(private authService: AuthService, private router: Router) { }


  ngOnInit(): void {

  }

  register() {
    console.log('register called');
    this.subscription = this.authService.register(this.user)
    .subscribe(data => {
      console.log(data);
      this.router.navigateByUrl('auth/login');
    });
    alert("User is registered")
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }
}
