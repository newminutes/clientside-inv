import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth-service';

@Injectable({
  providedIn: 'root'
})
export class LoggedInAuthGuard implements CanActivate {
  
  

  constructor(public router: Router, public authService: AuthService) {
    
  }

  canActivate() : boolean {
    if(this.authService.currentUser$.value === undefined){
      this.router.navigate(['auth/login']);
      return false;
    }
    console.log('try log: ');
    return true;
  }

}
