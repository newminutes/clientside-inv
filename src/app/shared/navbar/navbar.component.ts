import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../auth/auth-service';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../../pages/user/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() title: string;
  isNavbarCollapsed = true;
  loggedInUser$: BehaviorSubject<User>;


  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$;
    console.log(this.loggedInUser$);
  }

  logout(): void {
    console.log('logout called.');
    this.authService.logout();
    this.router.navigate(['auth/login']);
  }

}
