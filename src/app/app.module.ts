import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LayoutComponent } from '../app/layout/layout/layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './auth/login/login.component';
// import { RegisterComponent } from './auth/register/register.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { UsecaseComponent } from './pages/about/usecases/usecase/usecase.component';
import { MatchListComponent } from './pages/match/match-list/match-list.component';


import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { AlertModule } from './alert/alert.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    LayoutComponent,
    DashboardComponent,
    UsecasesComponent,
    UsecaseComponent,
    // LoginComponent,
    // RegisterComponent,
    MatchListComponent,
    // CommentCreateComponent,
    // CommentEditComponent,
    // CommentDetailComponent,
    // FighterListComponent,
    FooterComponent
  ],
  imports: [BrowserModule,BrowserAnimationsModule,
    ToastrModule.forRoot(), AppRoutingModule, HttpClientModule, NgbModule, FormsModule, AlertModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
